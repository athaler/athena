#include "../TauProcessorAlg.h"
#include "../TauRunnerAlg.h"
#include "../TauCellThinningAlg.h"
#include "../ClusterCellRelinkAlg.h"

DECLARE_COMPONENT( TauProcessorAlg )
DECLARE_COMPONENT( TauRunnerAlg )
DECLARE_COMPONENT( TauCellThinningAlg )
DECLARE_COMPONENT( ClusterCellRelinkAlg )
